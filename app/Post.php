<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'user_id', 'type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id', 'type_id',
    ];

    public $timestamps = true;
    
    /**
     * Get the options for the post.
     */
    public function options()
    {
        return $this->hasMany('App\Option');
    }

    /**
     * Get the user that owns the post.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the type that owns the post.
     */
    public function type()
    {
        return $this->belongsTo('App\Type');
    }
    
    /**
     * The posts that belong to the tags.
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

}
