<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Post;
use App\Option;
use App\Tag;
use Validator;
use App\Http\Resources\Post as PostResource;
   
class PostController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::with([
            'options',
            'user' =>function($query){$query->select('id','name');},
            'type',
            'tags',
        ])->get();
    
        return $this->sendResponse(PostResource::collection($post), 'Data retrieved successfully.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'user_id' => 'required',
            'type_id' => 'required',
            'title' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $post = Post::create($input);
        
        foreach($request->get('option') as $option) {
            $post->options()->create([
                'post_id'       => $post->id,
                'option_name'   => $option['option_name'],
                'option_value'  => $option['option_value'],
            ]);
        }

        $tagIds = self::addTag($request->get('tags'));
        $post->tags()->sync($tagIds);

        return $this->sendResponse(new PostResource($post), 'Data created successfully.');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::with([
            'options',
            'user' =>function($query){$query->select('id','name');},
            'type',
            'tags',
        ])->where('id', $id)->get();
  
        if (is_null($post)) {
            return $this->sendError('Data not found.');
        }

        return $this->sendResponse(new PostResource($post), 'Data retrieved successfully.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $input = $request->all();
        $post = Post::find($id);
        $validator = Validator::make($input, [
            'user_id' => 'required',
            'type_id' => 'required',
            'title' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $post->user_id = $request->get('user_id');
        $post->type_id = $request->get('type_id');
        $post->title = $request->get('title');
        $post->save();

        foreach($request->get('option') as $option) {
            if($option['id']) {
                $options = Option::find($option['id']);
                $options->option_name = $option['option_name'];
                $options->option_value = $option['option_value'];
                $options->save();
            } else {
                $post->options()->create([
                    'post_id'       => $post->id,
                    'option_name'   => $option['option_name'],
                    'option_value'  => $option['option_value'],
                ]);
            }

        }

        $tagIds = self::addTag($request->get('tags'));
        $post->tags()->sync($tagIds);
   
        return $this->sendResponse(new PostResource($post), 'Data updated successfully.');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
   
        return $this->sendResponse([], 'Data deleted successfully.');
    }

    public function addTag($tags) {
        $tagNames = explode(',',$tags);
        $tagIds = [];
        foreach($tagNames as $tagName)
        {
            $tag = Tag::firstOrCreate(['name'=>$tagName]);
            if($tag)
            {
              $tagIds[] = $tag->id;
            }
        }

        return $tagIds;
    }
}