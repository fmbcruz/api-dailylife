<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    protected $hidden = ['pivot'];

    /**
     * The tags that belong to the posts.
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post');
    }

}
